# ZONKOID

### Assignment
Zkuste naprogramovat kód, který bude každých 5 minut kontrolovat nové půjčky na Zonky tržišti a vypíše je.

Programové API Zonky tržiště je dostupné na adrese https://api.zonky.cz/loans/marketplace, dokumentace na http://docs.zonky.apiary.io/#

### Setup

#### Properties:
* **initialize.loans.first=true** - Pri spustení programu natiahne všetky aktuálne dostupné pôžičky zo Zonky API. Program 
spracuje len novo vzniknuté pôžičky. V opačnom prípade pri prvom spustení spracuje všetky dostupné pôžičky.

* **printing.data.handler=true** - Vypíše spracované pôžičky zo Zonky API
* **in.memory.data.handler=true** - Uloží spracované pôžičky zo Zonky API, ktoré sú potom dostupné na 
*BASE_URL/api/marketplace/loans?batch=5&offset= 0*
    * offset - nepovinné, default = 0
    * batch - povinné

### Run
mvn clean spring-boot:run