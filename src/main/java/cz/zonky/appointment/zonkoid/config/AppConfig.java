package cz.zonky.appointment.zonkoid.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.zonky.appointment.zonkoid.core.exceptions.RestTemplateResponseErrorHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
@EnableScheduling
@EnableAutoConfiguration
@ComponentScan(basePackages = {
        "cz.zonky.appointment.zonkoid.core",
        "cz.zonky.appointment.zonkoid.dataprovider",
        "cz.zonky.appointment.zonkoid.entrypoint"
})
public class AppConfig {

    @Value("${http.connect.timeout.seconds: 30}")
    private Integer connectTimeout;

    @Value("${http.read.timeout.seconds: 30}")
    private Integer readTimeout;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(connectTimeout))
                .setReadTimeout(Duration.ofSeconds(readTimeout))
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }

}
