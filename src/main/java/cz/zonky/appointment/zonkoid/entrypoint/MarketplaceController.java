package cz.zonky.appointment.zonkoid.entrypoint;

import cz.zonky.appointment.zonkoid.core.handler.InMemoryLoansDataHandler;
import cz.zonky.appointment.zonkoid.core.model.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/marketplace")
@ConditionalOnProperty(name = "in.memory.data.handler", havingValue = "true")
public class MarketplaceController {

    @Autowired
    private InMemoryLoansDataHandler inMemoryLoansDataHandler;

    /**
     * Return sublist of created loans
     */
    @GetMapping("/loans")
    public List<Loan> getAllLoans(@RequestParam(value = "offset", required = false) Integer offset,
                                  @RequestParam(value = "batch") int batchSize) {
        return inMemoryLoansDataHandler.getSavedLoans(offset != null ? offset : 0, batchSize);
    }
}
