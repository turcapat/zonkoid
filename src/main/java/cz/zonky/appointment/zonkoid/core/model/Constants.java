package cz.zonky.appointment.zonkoid.core.model;

public class Constants {
//    public static final String DATE_FORMAT_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String DATE_FORMAT_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String TIME_ZONE = "Europe/Prague";
}
