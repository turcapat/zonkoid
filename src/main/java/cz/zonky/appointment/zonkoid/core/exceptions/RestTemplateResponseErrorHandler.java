package cz.zonky.appointment.zonkoid.core.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.*;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {
    private static final Logger log = LoggerFactory.getLogger(RestTemplateResponseErrorHandler.class);


    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return (httpResponse.getStatusCode().series() == CLIENT_ERROR || httpResponse.getStatusCode().series() == SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {

        log.error("============================response begin==========================================");
        log.error("Status code  : {}", response.getStatusCode());
        log.error("Status text  : {}", response.getStatusText());
        log.error("Headers      : {}", response.getHeaders());
        log.error("Response body: {}", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        log.error("=======================response end=================================================");

        switch (response.getStatusCode().series()) {
            case CLIENT_ERROR:
                throw HttpClientErrorException.create(response.getStatusCode(), response.getStatusText(), response.getHeaders(), response.getBody().readAllBytes(), Charset.defaultCharset());
            case SERVER_ERROR:
                throw HttpServerErrorException.create(response.getStatusCode(), response.getStatusText(), response.getHeaders(), response.getBody().readAllBytes(), Charset.defaultCharset());
            default:
                throw new UnknownHttpStatusCodeException(response.getStatusCode().value(), response.getStatusText(), response.getHeaders(), response.getBody().readAllBytes(), Charset.defaultCharset());
        }
    }
}
