package cz.zonky.appointment.zonkoid.core.job;

import cz.zonky.appointment.zonkoid.core.handler.DataHandler;
import cz.zonky.appointment.zonkoid.core.model.Loan;
import cz.zonky.appointment.zonkoid.dataprovider.MarketplaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MarketplaceScheduler {
    private static final Logger logger = LoggerFactory.getLogger(MarketplaceScheduler.class);

    @Autowired
    private MarketplaceService marketplaceService;

    @Autowired(required = false)
    private List<DataHandler> dataHandlers;

    @Value("${zonky.loans.batch.size:25}")
    private Integer batchSize;

    @Value("${zonky.loans.historic.data.size:250}")
    private Integer historicDataSize;

    @Value("${initialize.loans.first:true}")
    private Boolean initializeLoansFirst;

    private boolean historyDataLoaded = false;

    /**
     * When application starts, get asynchronously all loans from Zonky API
     * Switch ON/OFF with "initialize.loans.first" property
     */
    @Async
    @EventListener(ContextRefreshedEvent.class)
    public void onApplicationEvent() {
        logger.info("Initializing historical loans started.");
        if (initializeLoansFirst && !historyDataLoaded) {
            marketplaceService.getLoans(historicDataSize, false);
        } else {
            logger.info("Historical loans initialized.");
        }
        historyDataLoaded = true;
        logger.info("Historical loans already initialized.");
    }

    /**
     * Periodicaly checks for new loans on Zonky API
     */
    @Scheduled(cron = "${cron.zonky.marketplace.loans:* */5 * * * *}")
    public void getLoansFromMarketplace() {
        if (!initializeLoansFirst || historyDataLoaded) {
            logger.info("Scheduled execution of zonkoid job started.");
            List<Loan> loans = marketplaceService.getLoans(batchSize, true);
            handleLoans(loans);
            logger.info("Scheduled execution of zonkoid job ended.");
        }
    }

    private void handleLoans(List<Loan> loans) {
        if (dataHandlers != null) {
            dataHandlers.forEach(handler -> handler.handleLoans(loans));
        }
    }
}
