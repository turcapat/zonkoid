package cz.zonky.appointment.zonkoid.core.handler;

import cz.zonky.appointment.zonkoid.core.model.Loan;

import java.util.List;

public interface DataHandler {

    void handleLoans (List<Loan> loans);
}
