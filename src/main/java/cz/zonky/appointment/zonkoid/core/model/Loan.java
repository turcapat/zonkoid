package cz.zonky.appointment.zonkoid.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Loan {

    private int id;
    private String url;
    private String name;
    private String story;
    private EN_Purpose purpose;
    private List<Photo> photos;
    private String nickName;
    private Integer termInMonths;
    private Double interestRate;
    private Double revenueRate;
    private Integer annuityWithInsurance;
    private String rating;
    private Boolean topped;
    private Integer amount;
    private String currency;
    private Integer remainingInvestment;
    private Integer reservedAmount;
    private Double investmentRate;
    private Boolean covered;
    private Date datePublished;
    private Boolean published;
    private Date deadline;
    private Integer investmentsCount;
    private Integer questionsCount;
    private String region;
    private String mainIncomeType;
    private Boolean insuranceActive;
    private List<InsuranceHistoryItem> insuranceHistory;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public EN_Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(EN_Purpose purpose) {
        this.purpose = purpose;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getTermInMonths() {
        return termInMonths;
    }

    public void setTermInMonths(Integer termInMonths) {
        this.termInMonths = termInMonths;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    public Double getRevenueRate() {
        return revenueRate;
    }

    public void setRevenueRate(Double revenueRate) {
        this.revenueRate = revenueRate;
    }

    public Integer getAnnuityWithInsurance() {
        return annuityWithInsurance;
    }

    public void setAnnuityWithInsurance(Integer annuityWithInsurance) {
        this.annuityWithInsurance = annuityWithInsurance;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Boolean getTopped() {
        return topped;
    }

    public void setTopped(Boolean topped) {
        this.topped = topped;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getRemainingInvestment() {
        return remainingInvestment;
    }

    public void setRemainingInvestment(Integer remainingInvestment) {
        this.remainingInvestment = remainingInvestment;
    }

    public Integer getReservedAmount() {
        return reservedAmount;
    }

    public void setReservedAmount(Integer reservedAmount) {
        this.reservedAmount = reservedAmount;
    }

    public Double getInvestmentRate() {
        return investmentRate;
    }

    public void setInvestmentRate(Double investmentRate) {
        this.investmentRate = investmentRate;
    }

    public Boolean getCovered() {
        return covered;
    }

    public void setCovered(Boolean covered) {
        this.covered = covered;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Integer getInvestmentsCount() {
        return investmentsCount;
    }

    public void setInvestmentsCount(Integer investmentsCount) {
        this.investmentsCount = investmentsCount;
    }

    public Integer getQuestionsCount() {
        return questionsCount;
    }

    public void setQuestionsCount(Integer questionsCount) {
        this.questionsCount = questionsCount;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMainIncomeType() {
        return mainIncomeType;
    }

    public void setMainIncomeType(String mainIncomeType) {
        this.mainIncomeType = mainIncomeType;
    }

    public Boolean getInsuranceActive() {
        return insuranceActive;
    }

    public void setInsuranceActive(Boolean insuranceActive) {
        this.insuranceActive = insuranceActive;
    }

    public List<InsuranceHistoryItem> getInsuranceHistory() {
        return insuranceHistory;
    }

    public void setInsuranceHistory(List<InsuranceHistoryItem> insuranceHistory) {
        this.insuranceHistory = insuranceHistory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return id == loan.id &&
                Objects.equals(url, loan.url) &&
                Objects.equals(name, loan.name) &&
                Objects.equals(story, loan.story) &&
                purpose == loan.purpose &&
                Objects.equals(photos, loan.photos) &&
                Objects.equals(nickName, loan.nickName) &&
                Objects.equals(termInMonths, loan.termInMonths) &&
                Objects.equals(interestRate, loan.interestRate) &&
                Objects.equals(revenueRate, loan.revenueRate) &&
                Objects.equals(annuityWithInsurance, loan.annuityWithInsurance) &&
                Objects.equals(rating, loan.rating) &&
                Objects.equals(topped, loan.topped) &&
                Objects.equals(amount, loan.amount) &&
                Objects.equals(currency, loan.currency) &&
                Objects.equals(remainingInvestment, loan.remainingInvestment) &&
                Objects.equals(reservedAmount, loan.reservedAmount) &&
                Objects.equals(investmentRate, loan.investmentRate) &&
                Objects.equals(covered, loan.covered) &&
                Objects.equals(datePublished, loan.datePublished) &&
                Objects.equals(published, loan.published) &&
                Objects.equals(deadline, loan.deadline) &&
                Objects.equals(investmentsCount, loan.investmentsCount) &&
                Objects.equals(questionsCount, loan.questionsCount) &&
                Objects.equals(region, loan.region) &&
                Objects.equals(mainIncomeType, loan.mainIncomeType) &&
                Objects.equals(insuranceActive, loan.insuranceActive) &&
                Objects.equals(insuranceHistory, loan.insuranceHistory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, url, name, story, purpose, photos, nickName, termInMonths, interestRate, revenueRate, annuityWithInsurance, rating, topped, amount, currency, remainingInvestment, reservedAmount, investmentRate, covered, datePublished, published, deadline, investmentsCount, questionsCount, region, mainIncomeType, insuranceActive, insuranceHistory);
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", story='" + story + '\'' +
                ", purpose=" + purpose +
                ", photos=" + photos +
                ", nickName='" + nickName + '\'' +
                ", termInMonths=" + termInMonths +
                ", interestRate=" + interestRate +
                ", revenueRate=" + revenueRate +
                ", annuityWithInsurance=" + annuityWithInsurance +
                ", rating='" + rating + '\'' +
                ", topped=" + topped +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", remainingInvestment=" + remainingInvestment +
                ", reservedAmount=" + reservedAmount +
                ", investmentRate=" + investmentRate +
                ", covered=" + covered +
                ", datePublished=" + datePublished +
                ", published=" + published +
                ", deadline=" + deadline +
                ", investmentsCount=" + investmentsCount +
                ", questionsCount=" + questionsCount +
                ", region='" + region + '\'' +
                ", mainIncomeType='" + mainIncomeType + '\'' +
                ", insuranceActive=" + insuranceActive +
                ", insuranceHistory=" + insuranceHistory +
                '}';
    }
}


