package cz.zonky.appointment.zonkoid.core.model;

public enum EN_Purpose {
    REFINANCING,
    AUTO_MOTO,
    EDUCATION,
    TRAVEL,
    ELECTRONICS,
    HEALTH,
    HOUSEHOLD,
    OWN_PROJECT,
    OTHER;


}
