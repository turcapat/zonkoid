package cz.zonky.appointment.zonkoid.core.handler;

import cz.zonky.appointment.zonkoid.core.model.Loan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@ConditionalOnProperty(name = "in.memory.data.handler", havingValue = "true")
public class InMemoryLoansDataHandler implements DataHandler {

    List<Loan> savedLoans = new ArrayList<>();

    /**
     * Prints all created loans
     *
     * @param loans from Zonky
     */
    @Override
    public void handleLoans(List<Loan> loans) {
        savedLoans.addAll(loans);
    }

    public List<Loan> getSavedLoans(int batchSize, int offset){
        return savedLoans.subList(offset, offset+batchSize);
    }
}
