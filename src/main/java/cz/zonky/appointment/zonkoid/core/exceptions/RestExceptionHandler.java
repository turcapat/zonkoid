package cz.zonky.appointment.zonkoid.core.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value = {IllegalStateException.class})
    protected ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request) {
        logger.error("Error calling zonkoid API.", ex);
        throw new IllegalStateException("Error calling zonkoid API.", ex);
    }
}

