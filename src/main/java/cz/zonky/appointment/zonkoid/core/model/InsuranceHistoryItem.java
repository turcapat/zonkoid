package cz.zonky.appointment.zonkoid.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InsuranceHistoryItem {
    private String insuranceHistoryItem;

    public String getInsuranceHistoryItem() {
        return insuranceHistoryItem;
    }

    public void setInsuranceHistoryItem(String insuranceHistoryItem) {
        this.insuranceHistoryItem = insuranceHistoryItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsuranceHistoryItem that = (InsuranceHistoryItem) o;
        return Objects.equals(insuranceHistoryItem, that.insuranceHistoryItem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(insuranceHistoryItem);
    }

    @Override
    public String toString() {
        return "InsuranceHistoryItem{" +
                "insuranceHistoryItem='" + insuranceHistoryItem + '\'' +
                '}';
    }
}
