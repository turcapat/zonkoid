package cz.zonky.appointment.zonkoid.core.handler;

import cz.zonky.appointment.zonkoid.core.model.Loan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "printing.data.handler", havingValue = "true")
public class PrintingLoansDataHandler implements DataHandler {

    /**
     * Prints all created loans
     * @param loans from Zonky
     */
    @Override
    public void handleLoans(List<Loan> loans) {
        loans.forEach(System.out::println);
    }
}
