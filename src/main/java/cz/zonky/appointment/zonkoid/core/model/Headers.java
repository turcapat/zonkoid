package cz.zonky.appointment.zonkoid.core.model;

public class Headers {
    public static final String X_PAGE = "X-Page";
    public static final String X_SIZE = "X-Size";
    public static final String X_ORDER = "X-Order";
}
