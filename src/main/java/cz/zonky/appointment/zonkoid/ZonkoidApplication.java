package cz.zonky.appointment.zonkoid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZonkoidApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZonkoidApplication.class, args);
	}
}
