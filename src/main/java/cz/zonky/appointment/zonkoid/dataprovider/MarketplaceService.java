package cz.zonky.appointment.zonkoid.dataprovider;

import cz.zonky.appointment.zonkoid.core.model.Constants;
import cz.zonky.appointment.zonkoid.core.model.Headers;
import cz.zonky.appointment.zonkoid.core.model.Loan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class MarketplaceService {
    private static final Logger logger = LoggerFactory.getLogger(MarketplaceService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${zonky.marketplace.url}")
    private String zonkyUrl;

    @Value("${zonky.loans.order.by}")
    private String orderBy;

    private Date latestDate = new Date(0L);
    private static final String PUBLISHED_DATE = "datePublished__gt";

    /**
     * Acquire loans from Zonky API (paginated call)
     * Get only loans that was published after configured date (latest date over all acquired loans)
     * */
    public List<Loan> getLoans(int batchSize, boolean withFilter) {
        ArrayList<Loan> loans = new ArrayList<>();
        int page = 0;
        boolean completed = false;
        Date dateFrom = latestDate;
        String url = withFilter ? buildUri(zonkyUrl,  dateFrom) : zonkyUrl;

        while (!completed) {
            ResponseEntity<List<Loan>> loansResponse = callZonkyApi(url, String.valueOf(page), batchSize);
            if (loansResponse != null && loansResponse.hasBody()) {
                List<Loan> actualBatch = loansResponse.getBody();
                loans.addAll(actualBatch);
                latestDate = parseLatestDate(actualBatch);
                completed = isCompleted(actualBatch.size(), batchSize);
                page++;
            } else {
                logger.error("Error getting loans from Zonky API.");
                break;
            }
        }
        return loans;
    }

    /**
     * Find latest date in batch, compare to overall latest date of loans, return later one
     * */
    private Date parseLatestDate(List<Loan> loans) {
        Date latestDateForBatch = loans.stream().map(Loan::getDatePublished).max(Date::compareTo).orElse(new Date(0L));
        return latestDate.after(latestDateForBatch) ? latestDate : latestDateForBatch;
    }

    /**
     * Builds an URL with query parameters
     * */
    private String buildUri(String url, Date publishedDate) {
        DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT_ISO_8601, Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone(Constants.TIME_ZONE));

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam(PUBLISHED_DATE, dateFormat.format(publishedDate));
        return builder.build().toUriString();
    }

    /**
     * Check if getting data can be completed.
     * Logic: If API returns less results then configured batch size then stop
     * */
    private boolean isCompleted(int actualBatchSize, int batchSize) {
        return actualBatchSize < batchSize;
    }

    /**
     * Actual call to the API
     * */
    private ResponseEntity<List<Loan>> callZonkyApi(String url, String page, int batchSize) {
        logger.info("Calling URL: {}", url);
        return restTemplate.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(getHeaders(page, String.valueOf(batchSize), orderBy)),
                new ParameterizedTypeReference<>() {
                });
    }

    private HttpHeaders getHeaders(String page, String size, String orderBy) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(Headers.X_PAGE, page);
        headers.set(Headers.X_SIZE, size);
        headers.set(Headers.X_ORDER, orderBy);
        return headers;
    }
}
