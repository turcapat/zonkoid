package cz.zonky.appointment.zonkoid;

import cz.zonky.appointment.zonkoid.core.job.MarketplaceScheduler;
import cz.zonky.appointment.zonkoid.dataprovider.MarketplaceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SchedulerTest {

    @Mock
    private MarketplaceService marketplaceService;

    @InjectMocks
    private MarketplaceScheduler scheduler = new MarketplaceScheduler();

    @Test
    public void startEvent() {
        ReflectionTestUtils.setField(scheduler, "initializeLoansFirst", true);
        ReflectionTestUtils.setField(scheduler, "historicDataSize", 100);

        scheduler.onApplicationEvent();
        verify(marketplaceService, times(1)).getLoans(100, false);

        scheduler.onApplicationEvent();
        verifyNoMoreInteractions(marketplaceService);
    }

    @Test
    public void getLoans() {
        ReflectionTestUtils.setField(scheduler, "initializeLoansFirst", false);
        ReflectionTestUtils.setField(scheduler, "batchSize", 10);

        scheduler.getLoansFromMarketplace();
        verify(marketplaceService, times(1)).getLoans(10, true);

        scheduler.getLoansFromMarketplace();
        verify(marketplaceService, times(2)).getLoans(10, true);
    }
}
