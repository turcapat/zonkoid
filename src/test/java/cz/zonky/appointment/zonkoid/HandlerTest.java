package cz.zonky.appointment.zonkoid;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.zonky.appointment.zonkoid.core.handler.PrintingLoansDataHandler;
import cz.zonky.appointment.zonkoid.core.job.MarketplaceScheduler;
import cz.zonky.appointment.zonkoid.core.model.Loan;
import cz.zonky.appointment.zonkoid.dataprovider.MarketplaceService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class HandlerTest {

    @Autowired
    @InjectMocks
    private MarketplaceScheduler scheduler;

    @MockBean
    private MarketplaceService marketplaceService;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private static final String LOAN_NAME = "Custom loan name";
    private static final String LOAN_URL = "Custom UTL";

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void out() {
        ReflectionTestUtils.setField(scheduler, "initializeLoansFirst", false);
        ReflectionTestUtils.setField(scheduler, "batchSize", 10);

        List<Loan> loans = new ArrayList<>();
        Loan loan = new Loan();
        loan.setId(7);
        loan.setName(LOAN_NAME);
        loan.setUrl(LOAN_URL);
        loans.add(loan);
        when(marketplaceService.getLoans(anyInt(), anyBoolean())).thenReturn(loans);

        // Keep current System.out with us
        PrintStream oldOut = System.out;
        // Create a ByteArrayOutputStream so that we can get the output from the call to print
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // Change System.out to point out to our stream
        System.setOut(new PrintStream(baos));

        scheduler.getLoansFromMarketplace();

        // Reset the System.out
        System.setOut(oldOut);
        // Our baos has the content from the print statement
        String output = new String(baos.toByteArray());

        // Add some assertions out output
        assertTrue(output.contains(LOAN_NAME));
        assertTrue(output.contains(LOAN_URL));
    }

    //@EnableWebMvc
    @Configuration
    @ComponentScan(basePackages = {
            "cz.zonky.appointment.zonkoid.core",
            "cz.zonky.appointment.zonkoid.dataprovider",
            "cz.zonky.appointment.zonkoid.entrypoint"
    })
    @PropertySource("classpath:test.properties")
    public static class ZonkoidTestConfig implements WebMvcConfigurer {

        @Bean
        public MarketplaceScheduler marketplaceScheduler() {
            return new MarketplaceScheduler();
        }

        @Bean
        public PrintingLoansDataHandler printingLoansDataHandler() {
            return new PrintingLoansDataHandler();
        }
    }
}