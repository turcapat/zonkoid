package cz.zonky.appointment.zonkoid.resttemplate;

import cz.zonky.appointment.zonkoid.core.exceptions.RestTemplateResponseErrorHandler;
import cz.zonky.appointment.zonkoid.core.model.Loan;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.support.RestGatewaySupport;

import java.util.List;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;


@RunWith(SpringRunner.class)
@RestClientTest
public class RestTemplateTest {


    @Autowired
    private RestTemplateBuilder builder;

    private MockRestServiceServer server;
    private RestTemplate restTemplate;

    @Before
    public void setUp() {
        RestGatewaySupport gateway = new RestGatewaySupport();
        restTemplate = this.builder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
        gateway.setRestTemplate(restTemplate);
        server = MockRestServiceServer.createServer(gateway);
    }

    @Test(expected = HttpClientErrorException.class)
    public void verifyRestErrorHandlerClientError() {

        this.server
                .expect(ExpectedCount.once(), requestTo("/zonky/api/loans/2"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.NOT_FOUND));

        restTemplate.getForObject("/zonky/api/loans/2", Loan.class);
        this.server.verify();
        this.server.reset();
    }

    @Test(expected = HttpServerErrorException.class)
    public void verifyRestErrorHandlerServerError() {
        this.server
                .expect(ExpectedCount.once(), requestTo("/zonky/api/loans/1"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.GATEWAY_TIMEOUT));

        restTemplate.getForObject("/zonky/api/loans/1", Loan.class);
        this.server.verify();
        this.server.reset();
    }
}