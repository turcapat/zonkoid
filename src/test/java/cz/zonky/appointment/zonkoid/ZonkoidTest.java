package cz.zonky.appointment.zonkoid;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.zonky.appointment.zonkoid.core.model.Headers;
import cz.zonky.appointment.zonkoid.core.model.Loan;
import cz.zonky.appointment.zonkoid.dataprovider.MarketplaceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ZonkoidTest {

    @Autowired
    @InjectMocks
    private MarketplaceService marketplaceService;

    @MockBean
    private RestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Value("${zonky.marketplace.url}")
    private String zonkyUrl;

    @Value("${zonky.loans.batch.size}")
    private Integer batchSize;

    @Value("${zonky.loans.order.by}")
    private String orderBy;

    @Before
    public void setUpStreams() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void verifyWithFilter() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = marketplaceService.getClass().getDeclaredMethod("buildUri", String.class, Date.class);
        method.setAccessible(true);
        String modifiedUri = (String)method.invoke(marketplaceService, zonkyUrl, new Date(0L));

        List<Loan> loans = readResource("/getLoans.json");
        ResponseEntity<List<Loan>> responseEntity = new ResponseEntity<>(loans, HttpStatus.OK);
        when(restTemplate.exchange(eq(modifiedUri), eq(HttpMethod.GET), any(HttpEntity.class), any(ParameterizedTypeReference.class))).thenReturn(responseEntity);

        List<Loan> items = marketplaceService.getLoans(batchSize, true);
        ArgumentCaptor<HttpEntity> httpEnityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        verify(restTemplate, times(1)).exchange(eq(modifiedUri), eq(HttpMethod.GET), httpEnityCaptor.capture(), any(ParameterizedTypeReference.class));

        HttpEntity httpEntityRequest = httpEnityCaptor.getValue();
        assertNotNull(httpEntityRequest);

        HttpHeaders headers = httpEntityRequest.getHeaders();
        assertEquals(batchSize, Integer.valueOf(headers.getFirst(Headers.X_SIZE)));
        assertEquals(0, Integer.parseInt(headers.getFirst(Headers.X_PAGE)));
        assertEquals(orderBy, headers.getFirst(Headers.X_ORDER));

        assertNotNull(items);
        assertEquals(20, items.size());
    }

    @Test
    public void verifyWithoutFilter() throws IOException {
        List<Loan> loans = readResource("/getLoans.json");
        ResponseEntity<List<Loan>> responseEntity = new ResponseEntity<>(loans, HttpStatus.OK);
        when(restTemplate.exchange(eq(zonkyUrl), eq(HttpMethod.GET), any(HttpEntity.class), any(ParameterizedTypeReference.class))).thenReturn(responseEntity);

        List<Loan> items = marketplaceService.getLoans(batchSize, false);
        ArgumentCaptor<HttpEntity> httpEnityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        verify(restTemplate, times(1)).exchange(eq(zonkyUrl), eq(HttpMethod.GET), httpEnityCaptor.capture(), any(ParameterizedTypeReference.class));

        HttpEntity httpEntityRequest = httpEnityCaptor.getValue();
        assertNotNull(httpEntityRequest);

        HttpHeaders headers = httpEntityRequest.getHeaders();
        assertEquals(batchSize, Integer.valueOf(headers.getFirst(Headers.X_SIZE)));
        assertEquals(0, Integer.parseInt(headers.getFirst(Headers.X_PAGE)));
        assertEquals(orderBy, headers.getFirst(Headers.X_ORDER));

        assertNotNull(items);
        assertEquals(20, items.size());
    }

    private List<Loan> readResource(String resource) throws IOException {
        InputStream stream = getClass().getResourceAsStream(resource);
        return objectMapper.readValue(stream, new TypeReference<List<Loan>>() {
        });
    }

    @Configuration
    @ComponentScan(basePackages = {
            "cz.zonky.appointment.zonkoid.core",
            "cz.zonky.appointment.zonkoid.dataprovider",
            "cz.zonky.appointment.zonkoid.entrypoint"
    })
    @PropertySource("classpath:test.properties")
    public static class ZonkoidTestConfig implements WebMvcConfigurer {

        @Bean
        public MarketplaceService marketplaceService() {
            return new MarketplaceService();
        }

    }
}